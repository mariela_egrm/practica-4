/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica4;

/**
 *
 * @author Mariela Esther Gomez Rivera C.I. 6680592
 */
public class pilas {
    private int arr[];
    private int tope;
    public pilas(){
        this.arr=new int[5];
        this.tope=-1;
    }
    public boolean isEmpty(){
        return(this.tope==-1);
    }
    public boolean isFull(){
        return(this.arr.length-1==this.tope);
    }
    public void push(int element){
        if(this.isFull()){
            int x[]=new int[this.arr.length+2];
            for(int i=0;i<this.arr.length;i++)
                x[i]=this.arr[i];
            this.arr=x;
        }
        this.arr[++tope]=element;
    }
    public int peek(){
        return arr[tope];
    }
    public int pop(){
        int temp=-1;
        if(!this.isEmpty()){
            temp=arr[tope];
            arr[tope--]=-1;
        }
        return temp;
    }
    
}
